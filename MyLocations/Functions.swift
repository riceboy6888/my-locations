//
//  Functions.swift
//  MyLocations
//
//  Created by Pascal Huynh on 25/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import Foundation
import Dispatch

func afterDelay(seconds: Double, closure: () -> ()) {
  // Int64(seconds * Double(NSEC_PER_SEC)) - Convert the delay into internal time format
  let when = dispatch_time(DISPATCH_TIME_NOW, Int64(seconds * Double(NSEC_PER_SEC)))
  dispatch_after(when, dispatch_get_main_queue(), closure)
}