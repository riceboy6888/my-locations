//
//  CategoryPickerViewController.swift
//  MyLocations
//
//  Created by Pascal Huynh on 20/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class CategoryPickerViewController: UITableViewController {
  // MARK: - instances variables

  var selectedCategoryName = ""
  var selectedIndexPath = NSIndexPath()
  let categories = [
    "No Category",
    "Apple Store",
    "Bar",
    "Bookstore",
    "Club",
    "Grocery Store",
    "Historic Building",
    "House",
    "Icecream Vendor",
    "Landmark",
    "Park"
  ]
  
  // MARK: - UITableViewDataSource
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell
    let categoryName = categories[indexPath.row]
    cell.textLabel.text = categoryName
    
    if categoryName == selectedCategoryName {
      cell.accessoryType = .Checkmark
      selectedIndexPath = indexPath
    } else {
      cell.accessoryType = .None
    }
    return cell
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return categories.count
  }
  
  // MARK: - UITableViewDelegate
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row != selectedIndexPath.row {
      if let newCell = tableView.cellForRowAtIndexPath(indexPath) {
        newCell.accessoryType = .Checkmark
      }
    
      if let oldCell = tableView.cellForRowAtIndexPath(selectedIndexPath) {
        oldCell.accessoryType = .None
      }
    
      selectedIndexPath = indexPath
    }
  }
  
  // MARK: - Segue
  // /!\ prepareForSegue is called before tableView.didSelectRowAtIndexPath
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // The identifier is added in Storyboard - Unwind Segue to Scene Exit Placeholder (left bar)
    if segue.identifier == "PickedCategory" {
      let cell = sender as UITableViewCell
      if let indexPath = tableView.indexPathForCell(cell) {
        selectedCategoryName = categories[indexPath.row]
      }
    }
  }
}
