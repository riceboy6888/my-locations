//
//  AppDelegate.swift
//  MyLocations
//
//  Created by Pascal Huynh on 17/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit
import CoreData

// Handle Core Data Fail Error
let MyManagedObjectContextSaveDidFailNotification = "MyManagedObjectContextSaveDidFailNotification"

// Global function
func fatalCoreDataError(error: NSError?) {
  if let error = error {
    // Output the error
    println("*** Fatal error: \(error), \(error.userInfo)")
  }
  // Post a notification
  NSNotificationCenter.defaultCenter().postNotificationName(MyManagedObjectContextSaveDidFailNotification, object: error)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  // Core Data Boilerplate
  // Create and lazily init a managedObjectContext (same code each time we use Core Data)
  // managedObjectContext is the object that is used to talk to Core Data
  // /!\ pass managedObjectContext with segue not by getting the AppDelegate context from other Class
  // because it will create strong dependencies
  lazy var managedObjectContext: NSManagedObjectContext = {
    // Create a NSURL object pointing at the folder DataModel.momd where our CoreData model is stored
    if let modelURL = NSBundle.mainBundle().URLForResource("DataModel", withExtension: "momd") {
      // Create a NSManagedObjectModel from the modelURL that represents the data model during runtime
      if let model = NSManagedObjectModel(contentsOfURL: modelURL) {
        // Create a NSPersistentStoreCoordinator that is in charge of the SQLite database
        let coordinator = NSPersistentStoreCoordinator( managedObjectModel: model)
        // Create a NSURL that points at the DataStore.sqlite file
        let urls = NSFileManager.defaultManager().URLsForDirectory( .DocumentDirectory, inDomains: .UserDomainMask)
        let documentsDirectory = urls[0] as NSURL
        let storeURL = documentsDirectory.URLByAppendingPathComponent("DataStore.sqlite")
        //println("\(storeURL)")
        
        var error: NSError?
        // Add the SQLite databse to the store coordinator
        if let store = coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil, error: &error) {
          // Create the NSManagedObjectContext that will be returned
          let context = NSManagedObjectContext()
          context.persistentStoreCoordinator = coordinator
          return context
        // If something went wrong print the error messages
        } else {
          println("Error adding persistent store at \(storeURL): \(error)")
        }
      } else {
        println("Error initializing model from: \(modelURL)")
      }
    } else {
      println("Could not find data model in app bundle")
    }
    abort()
  }()

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // Override point for customization after application launch.

    // Pass managedObjectContext to currentLocationViewController
    let tabBarController = window!.rootViewController as UITabBarController
    if let tabBarViewControllers = tabBarController.viewControllers {
      let currentLocationViewController = tabBarViewControllers[0] as CurrentLocationViewController
      currentLocationViewController.managedObjectContext = managedObjectContext
    }
    
    // Register the notification handler with NSNotificationCenter
    listenForFatalCoreDataotifications()

    return true
  }

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }

  func listenForFatalCoreDataotifications() {
    // Tell NSNotificationCenter that we want to be notified whenever a MyManagedObjectContextSaveDidFailNotification is posted
    NSNotificationCenter.defaultCenter().addObserverForName(MyManagedObjectContextSaveDidFailNotification, object: nil,
      queue: NSOperationQueue.mainQueue(),
      usingBlock: { notification in
        // Create a UIAlertController to show the message error
        let alert = UIAlertController(title: "Internal Error", message: "There was a fatal error in the app and it cannot continue.\n\n" + "Press OK to terminate the app. Sorry for the inconvenience.", preferredStyle: .Alert)
        
        // '_' is a wildcard and it means ignore the parameter
        // Add a 'OK' button to the alert view
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { _ in
          let exception = NSException(name: NSInternalInconsistencyException, reason: "Fatal Core Data error", userInfo: nil)
          exception.raise()
        }
        
        alert.addAction(action)
        
        // Present the alert
        self.viewControllerForShowingAlert().presentViewController(alert, animated: true, completion: nil)
      }
    )
  }
  
  // Function that finds a view controller that is currently visible
  // /!\ in this case we can't use the rootViewController because it is the tab bar controller and it is also hidden
  func viewControllerForShowingAlert() -> UIViewController {
    let rootViewController = self.window!.rootViewController!
    if let presentedViewController = rootViewController.presentedViewController {
      return presentedViewController
    } else {
      return rootViewController
    }
  }
}

