//
//  FirstViewController.swift
//  MyLocations
//
//  Created by Pascal Huynh on 17/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

class CurrentLocationViewController: UIViewController, CLLocationManagerDelegate {

  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var latitudeLabel: UILabel!
  @IBOutlet weak var longitudeLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var tagButton: UIButton!
  @IBOutlet weak var getButton: UIButton!
  
  // Will give the GPS coordinates
  let locationManager = CLLocationManager()
  var location: CLLocation?
  var updatingLocation = false
  var lastLocationError: NSError?
  
  // Reverse Geocoding
  let geocoder = CLGeocoder()
  var placemark: CLPlacemark?
  var performingReverseGeocoding = false
  var lastGeocodingError: NSError?
  
  // Timeout
  var timer: NSTimer?
  
  // CoreData
  var managedObjectContext: NSManagedObjectContext!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    updatelabels()
    configureGetButton()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func getLocation() {
    // Check the authorization status
    let authStatus: CLAuthorizationStatus = CLLocationManager.authorizationStatus()

    if authStatus == .NotDetermined {
      locationManager.requestWhenInUseAuthorization()
      return
    }
    
    if authStatus == .Denied || authStatus == .Restricted {
      showLocationServicesDeniedAlert()
      return
    }
    
    // Stop location fetching when the 'stop' button is pressed
    if updatingLocation {
      stopLocationManager()
    } else {
      location = nil
      lastLocationError = nil
      placemark = nil
      lastGeocodingError = nil
      startLocationManager()
    }
    
    updatelabels()
    configureGetButton()
  }
  
  func updatelabels() {
    if let location = location {
      // Convert Double to String
      latitudeLabel.text = String(format: "%.8f", location.coordinate.latitude)
      longitudeLabel.text = String(format: "%.8f", location.coordinate.longitude)
      tagButton.hidden = false
      
      // Update the address label only once a location is found
      if let placemark = placemark {
        addressLabel.text = stringFromPlacemark(placemark)
      } else if performingReverseGeocoding {
        addressLabel.text = "Searching for Address..."
      } else if lastGeocodingError != nil {
        addressLabel.text = "Error Finding Address"
      } else {
        addressLabel.text = "No Address found"
      }
      
    } else {
      latitudeLabel.text = ""
      longitudeLabel.text = ""
      addressLabel.text = ""
      tagButton.hidden = true
      
      var statusMessage: String
      if let error = lastLocationError {
        if error.domain == kCLErrorDomain && error.code == CLError.Denied.rawValue {
          statusMessage = "Location Services Disabled"
        } else {
          statusMessage = "Error Getting Location"
        }
      } else if !CLLocationManager.locationServicesEnabled() {
        statusMessage = "Location Services Disabled"
      } else if updatingLocation {
        statusMessage = "Searching..."
      } else {
        statusMessage = "Tap 'Get My Location' to start"
      }
      messageLabel.text = statusMessage
    }
  }
  
  func startLocationManager() {
    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
      locationManager.startUpdatingLocation()
      updatingLocation = true
      
      // Send a message to 'didTimeOut' after 60 seconds
      timer = NSTimer.scheduledTimerWithTimeInterval(60, target: self, selector: Selector("didTimeOut"), userInfo: nil, repeats: false)
    }
  }
  
  func stopLocationManager() {
    if updatingLocation {
      // Cancel the timer in case the location manager is stopped before the time-out fires. 
      // This happens when an accurate enough location is found within one minute after starting
      // or when the user tapped the Stop button
      if let timer = timer {
        timer.invalidate()
      }
      locationManager.stopUpdatingLocation()
      locationManager.delegate = nil
      updatingLocation = false
    }
  }
  
  func configureGetButton() {
    if updatingLocation {
      getButton.setTitle("Stop", forState: .Normal)
    } else {
      getButton.setTitle("Get My Location", forState: .Normal)
    }
  }
  
  func stringFromPlacemark(placemark: CLPlacemark) -> String {
    // subThoroughfare is the house number
    // thoroughfare is the street name
    // locality is the city
    // administrativeArea is the state or province
    return
      "\(placemark.subThoroughfare) \(placemark.thoroughfare)\n" +
      "\(placemark.locality) \(placemark.administrativeArea) " +
      "\(placemark.postalCode)"
  }
  
  func didTimeOut() {
    println("*** Timeout")
    
    if location == nil {
      stopLocationManager()
      // Custom error message in 'MyLocationsErrorDomain' for timeout issue
      lastLocationError = NSError(domain: "MyLocationsErrorDomain", code: 1, userInfo: nil)
      updatelabels()
      configureGetButton()
    }
  }
  
  // MARK: - CLLocationManagerDelegate
  
  func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
    println("didFailWithError \(error)")
    
    // CLError.LocationUnknown - The location is currently unknown, but CL will keep trying
    // CLError.Denied - The user decliend the app to use location services
    // CLError.Network - There was a network related error
    if error.code == CLError.LocationUnknown.rawValue {
      return
    }
    
    lastLocationError = error
    stopLocationManager()
    updatelabels()
    configureGetButton()
  }
  
  func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
    let newLocation = locations.last as CLLocation
    println("didUpdateLocations \(newLocation)")
    
    // If the time at which the location object was determined is too long ago
    // 5 seconds in this case), then this is a so-called cached result.
    // Ignore the location if it is too old
    if newLocation.timestamp.timeIntervalSinceNow < -5 {
      return
    }
    
    // Ignore invalid measurements on horizontalAccuracy
    if newLocation.horizontalAccuracy < 0 {
      return
    }
    
    // Calculate the distance between the new reading and the previous reading
    // If no reading then distance = DBL_MAX
    var distance = CLLocationDistance(DBL_MAX)
    if let location = location {
      distance = newLocation.distanceFromLocation(location)
    }
    
    // Determine if the new reading is more useful than the previous one
    // We go on if the newLocation is more accurate than the previous one
    if location == nil || location!.horizontalAccuracy > newLocation.horizontalAccuracy {
      // Wipe the last location error when a location is found
      lastLocationError = nil
      location = newLocation
      updatelabels()
      
      // If the new location’s accuracy is equal to or better than the desired accuracy, 
      // you can call it a day and stop asking the location manager for updates
      if newLocation.horizontalAccuracy <= locationManager.desiredAccuracy {
        println("*** We're done!")
        stopLocationManager()
        configureGetButton()
        
        // Force reverse geocoding for the final location and coordinates
        // If distance is 0 that means the location is the same as the previous point
        // So there is no need to perform a reverse geocoding
        if distance > 0 {
          performingReverseGeocoding = false
        }
      }
      
      if !performingReverseGeocoding {
        println("*** Going to geocode")
        performingReverseGeocoding = true
        geocoder.reverseGeocodeLocation(location, completionHandler: {
          placemarks, error in
          //println("*** Found placemarks: \(placemarks), error: \(error)")
          println("*** Found placemarks")
          self.lastGeocodingError = nil

          if error == nil && !placemarks.isEmpty {
            self.placemark = placemarks.last as? CLPlacemark
          } else {
            self.placemark = nil
          }
          
          self.performingReverseGeocoding = false
          self.updatelabels()
        })
      }
    } else if distance < 1.0 {
      // If the coordinates from the current reading is not sugnificantly different from the previous reading
      // (here 1.0 meter) and it has been more than 10 seconds then it is a good point to stop the location manager
      // Tweak for iPod Touch because of its accuracy of +/- 100 meters
      let timeInterval = newLocation.timestamp.timeIntervalSinceDate(location!.timestamp)
      
      if timeInterval > 10 {
        println("*** Force done!")
        stopLocationManager()
        updatelabels()
        configureGetButton()
      }
    }
  }

  func showLocationServicesDeniedAlert() {
    let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable location services for this app in Settings.", preferredStyle: .Alert)
    let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
    alert.addAction(okAction)
    presentViewController(alert, animated: true, completion: nil)
  }

  // MARK: - Segue
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "TagLocation" {
      let navigationController = segue.destinationViewController as UINavigationController
      let controller = navigationController.topViewController as LocationDetailsViewController
      // Safe to unwrap location here because the "Tag Location" button appears only when there a location defined
      controller.coordinate = location!.coordinate
      controller.placemark = placemark
      // Pass managedObjectContext to LocationDetailsViewController
      controller.managedObjectContext = managedObjectContext
    }
  }
  
}

