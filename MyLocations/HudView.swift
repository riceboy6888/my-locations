//
//  HudView.swift
//  MyLocations
//
//  Created by Pascal Huynh on 20/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class HudView: UIView {

  // MARK: - instances variables
  
  var text = ""

  // Convenience constructor is always a class method
  // Call: let hudView = HudView.hudInView(parentView, animated: true)
  class func hudInView(view: UIView, animated: Bool) -> HudView {
    let hudView = HudView(frame: view.bounds)
    hudView.opaque = false
    
    view.addSubview(hudView)
    // Avoid user interaction with the screen when the HUD is showing
    view.userInteractionEnabled = false
    
    hudView.showAnimated(animated)
    return hudView
  }
  
  // The drawRect() method is invoked whenever UIKit wants your view to redraw itself. 
  // Recall that everything in iOS is event-driven. You don’t draw anything on the screen 
  // unless UIKit sends you the drawRect() event. That means you should never call drawRect() yourself.
  // Instead, if you want to force your view to redraw then you should send it the setNeedsDisplay() 
  // message. UIKit will then trigger a drawRect() event when it is ready to perform the drawing.
  override func drawRect(rect: CGRect) {
    // Draw a rounded rectangle in the center of the screen
    let boxWidth: CGFloat = 96
    let boxHeight: CGFloat = boxWidth
    
    // bounds.size gives the size of the screen
    let boxRect = CGRect(x: round((bounds.size.width - boxWidth) / 2),
                         y: round((bounds.size.height - boxHeight) / 2),
                         width: boxWidth,
                         height: boxHeight)
    
    let roundedRect = UIBezierPath(roundedRect: boxRect, cornerRadius: 10)
    UIColor(white: 0.3, alpha: 0.8).setFill()
    roundedRect.fill()
    
    // Position and draw the checkmark image
    if let image = UIImage(named: "Checkmark") {
      let imagePoint = CGPoint(x: center.x - round(image.size.width / 2),
                               y: center.y - round(image.size.height / 2) - boxHeight / 8)
      image.drawAtPoint(imagePoint)
    }
    
    // Position and draw the text
    // Define how the text will look like (via a dictionary)
    let attribs = [ NSFontAttributeName: UIFont.systemFontOfSize(16.0),
                    NSForegroundColorAttributeName: UIColor.whiteColor()]
    // Get the size of the text
    let textSize = text.sizeWithAttributes(attribs)

    let textPoint = CGPoint(x: center.x - round(textSize.width / 2),
                            y: center.y - round(textSize.height / 2) + boxHeight / 4)
    text.drawAtPoint(textPoint, withAttributes: attribs)
  }
  
  func showAnimated(animated: Bool) {
    if animated {
      // Set the initial state of the view
      alpha = 0 // Transparent
      transform = CGAffineTransformMakeScale(1.3, 1.3)
      
      // Set up the animation duration - Old version
      //UIView.animateWithDuration(0.3, animations: {
      //  // Set the new state
      //  self.alpha = 1 // Opaque
      //  self.transform = CGAffineTransformIdentity
      //})
      UIView.animateWithDuration(0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: UIViewAnimationOptions(0), animations: {
        self.alpha = 1 // Opaque
        self.transform = CGAffineTransformIdentity
      }, completion: nil)
    }
  }
}
