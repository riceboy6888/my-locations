//
//  LocationDetailsViewController.swift
//  MyLocations
//
//  Created by Pascal Huynh on 18/11/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

// Private means it's only visible inside the LocationDetailsViewController.swift file
private let dateFormatter: NSDateFormatter = {
  let formatter = NSDateFormatter()
  formatter.dateStyle = .MediumStyle
  formatter.timeStyle = .ShortStyle
  println("*** Should Only Appear Once")
  return formatter
}()

class LocationDetailsViewController: UITableViewController {
  // MARK: - IBOutlet variables
  
  @IBOutlet weak var descriptionTextView: UITextView!
  @IBOutlet weak var categoryLabel: UILabel!
  @IBOutlet weak var latitudeLabel: UILabel!
  @IBOutlet weak var longitudeLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var dateLabel: UILabel!

  // MARK: - instances variables
  
  var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
  var placemark: CLPlacemark?
  var descriptionText = ""
  var categoryName = "No Category"
  // CoreData
  var managedObjectContext: NSManagedObjectContext!
  var date = NSDate()

  // MARK: Overridden functions
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    descriptionTextView.text = descriptionText
    categoryLabel.text = categoryName
    
    latitudeLabel.text = String(format: "%.8f", coordinate.latitude)
    longitudeLabel.text = String(format: "%.8f", coordinate.longitude)
    
    if let placemark = placemark {
      addressLabel.text = stringFromPlacemark(placemark)
    } else {
      addressLabel.text = "No Address Found"
    }
    
    dateLabel.text = formatDate(date)
    
    // The ':' means that the method takes a single parameter (UIGestureRecognizer)
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard:"))
    gestureRecognizer.cancelsTouchesInView = false
    tableView.addGestureRecognizer(gestureRecognizer)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    // 30 points are the 15 points margin on each side
    descriptionTextView.frame.size.width = view.frame.size.width - 30
  }
  
  func hideKeyboard(gestureRecognizer: UIGestureRecognizer) {
    // Get where the tap happened
    let point = gestureRecognizer.locationInView(tableView)
    let indexPath = tableView.indexPathForRowAtPoint(point)
    
    // Keep the keyboard active as long as the description cell is tap
    // /!\ indexPath can be nil if the tap happened in between two sections or on the section header
    if indexPath != nil && indexPath!.section == 0 && indexPath!.row == 0 {
      return
    }
    // Otherwise hide the keyboard
    descriptionTextView.resignFirstResponder()
  }
  
  // MARK: - Custom methods
  
  func stringFromPlacemark(placemark: CLPlacemark) -> String {
    return
      "\(placemark.subThoroughfare) \(placemark.thoroughfare), " +
      "\(placemark.locality), " +
      "\(placemark.administrativeArea) \(placemark.postalCode), " +
      "\(placemark.country)"
  }
  
  func formatDate(date: NSDate) -> String {
    return dateFormatter.stringFromDate(date)
  }
  
  // MARK: - IBAction functions
  
  @IBAction func done() {
    let hudView = HudView.hudInView(navigationController!.view, animated: true)
    hudView.text = "Tagged"
    
    // Create a new Location object via Core Data
    // Location is the name of the entity in Core Data
    let location = NSEntityDescription.insertNewObjectForEntityForName("Location", inManagedObjectContext: managedObjectContext) as Location
    
    // Set the Location object properties
    location.locationDescription = descriptionText
    location.category = categoryName
    location.latitude = coordinate.latitude
    location.longitude = coordinate.longitude
    location.date = date
    location.placemark = placemark
    
    // Save the context to the Data Store
    var error: NSError?
    if !managedObjectContext.save(&error) {
      fatalCoreDataError(error)
      return
    }
    
    // Close the HUD after 0.6 seconds
    afterDelay(0.6) {
      self.dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  @IBAction func cancel() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  // MARK: - UITableViewDelegate
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 0 && indexPath.row == 0 {
      return 88
    } else if indexPath.section == 2 && indexPath.row == 2 {
      // A frame has an origin CGPoint(x, y) and a size CGSize(width, height)
      // All UIView objects have a frame, that's how we can position views on the screen programmatically
      
      // view.bounds.size.width is the width of the screen
      addressLabel.frame.size = CGSize(width: view.bounds.size.width - 115, height: 10000)

      // Resize the label back to the proper height
      addressLabel.sizeToFit()
      
      // With sizeToFit() the label is too fit so we apply some adjusments on the x position + margin
      addressLabel.frame.origin.x = view.bounds.size.width - addressLabel.frame.size.width - 15
      
      // And on the height + margin
      return addressLabel.frame.size.height + 20
      
      // Frame VS. Bounds
      // The frame describes the position and size of a view in its parent view. If you want to put a 150×50
      // label at position X: 100, Y: 30, then its frame is (100, 30, 150, 50). To move a view from one 
      // position to another, you change its frame property (or its center property, which in turn will modify the frame).
      
      // Where the frame describes the outside of the view, the bounds describe the inside. The X and Y 
      // coordinates of the bounds are (0, 0) and the width and height will be the same as the frame. 
      // So for the label example, its bounds are (0, 0, 150, 50). It’s a matter of perspective.
    } else {
      return 44
    }
  }
  
  // Limits the taps to the first two sections, the third one only displays read-only information
  override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
    if indexPath.section == 0 || indexPath.section == 1 {
      return indexPath
    } else {
      return nil
    }
  }
  
  // Give focus to the description text view
  // No need to specify anything on Category and Photo because it is handled by Segues
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.section == 0 && indexPath.row == 0 {
      descriptionTextView.becomeFirstResponder()
    }
  }
  
  // MARK: - Segue
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "PickCategory" {
      let controller = segue.destinationViewController as CategoryPickerViewController
      controller.selectedCategoryName = categoryName
    }
  }
  
  // Unwind segue /!\ sourceViewController and not destinationViewController
  @IBAction func categoryPickerDidPickCategory(segue: UIStoryboardSegue) {
    // Get the controller that sent the segue
    let controller = segue.sourceViewController as CategoryPickerViewController
    categoryName = controller.selectedCategoryName
    categoryLabel.text = categoryName
  }
}

// MARK: - Extension UITextViewDelegate

extension LocationDetailsViewController: UITextViewDelegate {
  func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
    descriptionText = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
    return true
  }
  
  func textViewDidEndEditing(textView: UITextView) {
    descriptionText = textView.text
  }
}
